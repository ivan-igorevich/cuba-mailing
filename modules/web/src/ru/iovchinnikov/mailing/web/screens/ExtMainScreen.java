package ru.iovchinnikov.mailing.web.screens;

import com.haulmont.cuba.gui.Screens;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.gui.components.Timer;
import com.haulmont.cuba.gui.screen.Subscribe;
import com.haulmont.cuba.gui.screen.UiController;
import com.haulmont.cuba.gui.screen.UiDescriptor;
import com.haulmont.cuba.security.global.UserSession;
import com.haulmont.cuba.web.app.main.MainScreen;
import ru.iovchinnikov.mailing.service.MessageService;
import ru.iovchinnikov.mailing.web.screens.message.MessageBrowse;

import javax.inject.Inject;

@UiController("main")
@UiDescriptor("ext-main-screen.xml")
public class ExtMainScreen extends MainScreen {

@Inject private MessageService messageService;
@Inject private UserSession userSession;
@Inject private Action actOpenMsg;
@Inject
private Screens screens;


    @Subscribe
    private void onBeforeShow(BeforeShowEvent event) {
        checkMessages();
    }

    @Subscribe("actOpenMsg")
    private void onActOpenMsg(Action.ActionPerformedEvent event) {
        screens.create(MessageBrowse.class).show();
    }

    public void updateMessages(Timer source) {
        checkMessages();
    }

    private void checkMessages() {
        long msgCount = messageService.getUnreadMsgCount(userSession.getUser());
        String msgCountCaption = (msgCount > 0) ? String.format("(%d)", msgCount) : "";
        actOpenMsg.setCaption(msgCountCaption);
    }
}