package ru.iovchinnikov.mailing.service;

import com.haulmont.cuba.security.entity.User;

import java.util.LinkedHashSet;
import java.util.List;

public interface UserService {
    String NAME = "mailing_UserService";

    User getUserByLogin(String login_lc);
    List<User> getUsersByGroup(String group);
    LinkedHashSet<User> getUsersByRole(String secRoleName);
}