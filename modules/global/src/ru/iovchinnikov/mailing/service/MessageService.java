package ru.iovchinnikov.mailing.service;

import com.haulmont.cuba.security.entity.User;
import ru.iovchinnikov.mailing.entity.Message;

import java.util.Set;

public interface MessageService {
    String NAME = "mailing_MessageService";

    long getUnreadMsgCount(User user);
    void send(User sender, User recipient, String subject, String text);
    void setIsRead(Message msg);
    void switchDeleted(Set<Message> msg, User user);
    void systemSendToRole(String role, String subject, String text);
    void systemSendToLogin(String login, String subject, String text);
    void systemSendToGroup(String group, String subject, String text);
}