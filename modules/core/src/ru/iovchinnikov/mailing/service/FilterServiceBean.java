package ru.iovchinnikov.mailing.service;

import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.FilterEntity;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(FilterService.NAME)
public class FilterServiceBean implements FilterService {
    @Inject private Metadata metadata;
    @Inject private Messages messages;

    @Override
    public FilterEntity getEmptyFilter(User user) {
        FilterEntity filterEntity = metadata.create(FilterEntity.class);
        filterEntity.setName(messages.getMainMessage("All messages"));
        String filterXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<filter>\n" +
                "  <and>\n" +
                "    <or name=\"group\" unary=\"true\" width=\"1\">\n" +
                "      <c name=\"recipient\" class=\"com.haulmont.cuba.security.entity.User\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.recipient.id = :component$filter.recipient86152]]>\n" +
                "        <param name=\"component$filter.recipient86152\" javaClass=\"com.haulmont.cuba.security.entity.User\">" + user.getId() + "</param>\n" +
                "      </c>\n" +
                "      <c name=\"sender\" class=\"com.haulmont.cuba.security.entity.User\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.sender.id = :component$filter.sender46112]]>\n" +
                "        <param name=\"component$filter.sender46112\" javaClass=\"com.haulmont.cuba.security.entity.User\">" + user.getId() + "</param>\n" +
                "      </c>\n" +
                "    </or>\n" +
                "  </and>\n" +
                "</filter>\n";
        filterEntity.setXml(filterXml);
        return filterEntity;
    }

    @Override
    public FilterEntity getSenderFilter(User user) {
        FilterEntity filterEntity = metadata.create(FilterEntity.class);
        filterEntity.setName(messages.getMainMessage("Sent by me"));
        String filterXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<filter>\n" +
                "  <and>\n" +
                "    <c name=\"sender\" class=\"com.haulmont.cuba.security.entity.User\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.sender.id = :component$filter.sender57210]]>\n" +
                "      <param name=\"component$filter.sender57210\" javaClass=\"com.haulmont.cuba.security.entity.User\">" + user.getId() + "</param>\n" +
                "    </c>\n" +
                "    <c name=\"meta.deletedBySender\" class=\"java.lang.Boolean\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.meta.deletedBySender = :component$filter.meta_deletedBySender19931]]>\n" +
                "      <param name=\"component$filter.meta_deletedBySender19931\" javaClass=\"java.lang.Boolean\">false</param>\n" +
                "    </c>\n" +
                "  </and>\n" +
                "</filter>\n";
        filterEntity.setXml(filterXml);
        return filterEntity;
    }

    @Override
    public FilterEntity getReceiverFilter(User user) {
        FilterEntity filterEntity = metadata.create(FilterEntity.class);
        filterEntity.setName(messages.getMainMessage("Received by me"));
        String filterXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<filter>\n" +
                "  <and>\n" +
                "    <c name=\"recipient\" class=\"com.haulmont.cuba.security.entity.User\" hidden=\"true\" required=\"true\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.recipient.id = :component$filter.recipient91151]]>\n" +
                "      <param name=\"component$filter.recipient91151\" javaClass=\"com.haulmont.cuba.security.entity.User\">" + user.getId() + "</param>\n" +
                "    </c>\n" +
                "    <c name=\"meta.deletedByRecipient\" class=\"java.lang.Boolean\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.meta.deletedByRecipient = :component$filter.meta_deletedByRecipient94936]]>\n" +
                "      <param name=\"component$filter.meta_deletedByRecipient94936\" javaClass=\"java.lang.Boolean\">false</param>\n" +
                "    </c>\n" +
                "  </and>\n" +
                "</filter>\n";
        filterEntity.setXml(filterXml);
        return filterEntity;
    }

    @Override
    public FilterEntity getRecycleBinFilter(User user) {
        FilterEntity filterEntity = metadata.create(FilterEntity.class);
        filterEntity.setName(messages.getMainMessage("All messages"));
        String filterXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<filter>\n" +
                "  <and>\n" +
                "    <or name=\"group\" unary=\"true\" width=\"1\">\n" +
                "      <and name=\"group\" unary=\"true\" width=\"1\">\n" +
                "        <c name=\"recipient\" class=\"com.haulmont.cuba.security.entity.User\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.recipient.id = :component$filter.recipient86152]]>\n" +
                "          <param name=\"component$filter.recipient86152\" javaClass=\"com.haulmont.cuba.security.entity.User\">" + user.getId() + "</param>\n" +
                "        </c>\n" +
                "        <c name=\"meta.deletedByRecipient\" class=\"java.lang.Boolean\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.meta.deletedByRecipient = :component$filter.meta_deletedByRecipient94936]]>\n" +
                "          <param name=\"component$filter.meta_deletedByRecipient94936\" javaClass=\"java.lang.Boolean\">true</param>\n" +
                "        </c>\n" +
                "      </and>\n" +
                "      <and name=\"group\" unary=\"true\" width=\"1\">\n" +
                "        <c name=\"sender\" class=\"com.haulmont.cuba.security.entity.User\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.sender.id = :component$filter.sender46112]]>\n" +
                "          <param name=\"component$filter.sender46112\" javaClass=\"com.haulmont.cuba.security.entity.User\">" + user.getId() + "</param>\n" +
                "        </c>\n" +
                "        <c name=\"meta.deletedBySender\" class=\"java.lang.Boolean\" operatorType=\"EQUAL\" width=\"1\" type=\"PROPERTY\"><![CDATA[e.meta.deletedBySender = :component$filter.meta_deletedBySender18730]]>\n" +
                "          <param name=\"component$filter.meta_deletedBySender18730\" javaClass=\"java.lang.Boolean\">true</param>\n" +
                "        </c>\n" +
                "      </and>\n" +
                "    </or>\n" +
                "  </and>\n" +
                "</filter>\n";
        filterEntity.setXml(filterXml);
        return filterEntity;
    }
}
