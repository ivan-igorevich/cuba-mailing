package ru.iovchinnikov.mailing.service;

import com.haulmont.cuba.core.global.CommitContext;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;
import ru.iovchinnikov.mailing.config.SystemUserConfig;
import ru.iovchinnikov.mailing.entity.Contents;
import ru.iovchinnikov.mailing.entity.Message;
import ru.iovchinnikov.mailing.entity.MetaInfo;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Service(MessageService.NAME)
public class MessageServiceBean implements MessageService {

    @Inject private DataManager dataManager;
    @Inject private UserService userService;
    @Inject private SystemUserConfig systemUserConfig;

    @Override
    public long getUnreadMsgCount(User user) {
        LoadContext<Message> ctx = LoadContext.create(Message.class);
        ctx.setQuery(LoadContext.createQuery("select e from mailing_Message e where e.recipient.id = :current and " +
                                                        "e.meta.isRead = false and e.meta.deletedByRecipient = false")
                .setParameter("current", user.getId()));
        return dataManager.getCount(ctx);
    }

    @Override
    public void send(User sender, User recipient, String subject, String text) {
        // To send a message we need to create it and put it to the same context with related entities
        CommitContext commitContext = new CommitContext();
        MetaInfo meta = dataManager.create(MetaInfo.class);
        Contents contents = dataManager.create(Contents.class);
        Message msg = dataManager.create(Message.class);

        msg.setRecipient(recipient);
        msg.setSender(sender);
        msg.setSubject(subject);
        msg.setMeta(meta);
        msg.setContents(contents);

        contents.setText(text);
        contents.setMessage(msg);

        meta.initNewItem();
        meta.setSent(true);
        meta.setMessage(msg);

        // As we created entities and filled them with data, we put them to context and commit in one transaction
        commitContext.addInstanceToCommit(msg, "message-view");
        commitContext.addInstanceToCommit(meta, "metaInfo-view");
        commitContext.addInstanceToCommit(contents, "contents-view");
        dataManager.commit(commitContext);
    }

    @Override
    public void setIsRead(Message msg) {
        msg = dataManager.reload(msg, "message-view");
        msg.getMeta().setIsRead(true);
        dataManager.commit(msg.getMeta());
    }

    @Override
    public void switchDeleted(Set<Message> msgSet, User user) {
        for (Message msg : msgSet) {
            msg = dataManager.reload(msg, "message-view");
            if (user.equals(msg.getRecipient()))
                if (msg.getMeta().getDeletedByRecipient().equals(false))
                    msg.getMeta().setDeletedByRecipient(true);
                else
                    msg.getMeta().setDeletedByRecipient(false);
            else if (user.equals(msg.getSender()))
                if (msg.getMeta().getDeletedBySender().equals(false))
                    msg.getMeta().setDeletedBySender(true);
                else
                    msg.getMeta().setDeletedBySender(false);
            else
                throw new RuntimeException(String.format(
                        "Development Exception, unexpected user: %s\n" +
                                "Deleting messages is allowed for recipient or sender only.", user));
            dataManager.commit(msg.getMeta());
        }
    }

    @Override
    public void systemSendToRole(String role, String subject, String text) {
        for (User u : userService.getUsersByRole(role)) {
            send(systemUserConfig.getSystemUser(),
                    u,
                    subject,
                    text);
        }
    }

    @Override
    public void systemSendToLogin(String login, String subject, String text) {
        send(systemUserConfig.getSystemUser(),
                userService.getUserByLogin(login),
                subject,
                text);
    }

    @Override
    public void systemSendToGroup(String group, String subject, String text) {
        List<User> checkedGroup = userService.getUsersByGroup("Company");
        for (User u : checkedGroup) {
            send(systemUserConfig.getSystemUser(),
                    u,
                    subject,
                    text);
        }

    }
}