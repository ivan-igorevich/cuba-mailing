package ru.iovchinnikov.mailing.service;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.LoadContext;
import com.haulmont.cuba.security.entity.Group;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;

@Service(UserService.NAME)
public class UserServiceBean implements UserService {

    @Inject private DataManager dataManager;

    @Override
    public User getUserByLogin(String login_lc) {
        LoadContext<User> ctx = LoadContext.create(User.class);
        ctx.setQuery(LoadContext.createQuery("select e from sec$User e where e.loginLowerCase = :param")
                .setParameter("param", login_lc));
        return dataManager.load(ctx);
    }

    @Override
    public List<User> getUsersByGroup(String name) {
        LoadContext<Group> grpCtx = LoadContext.create(Group.class);
        grpCtx.setQuery(LoadContext.createQuery("select e from sec$Group e where e.name = :grp")
                .setParameter("grp", name));
        Group grp = dataManager.load(grpCtx);
        if (grp == null) return null;
        LoadContext<User> ctx = LoadContext.create(User.class);
        ctx.setQuery(LoadContext.createQuery("select e from sec$User e where e.group = :grp")
                .setParameter("grp", grp));
        return dataManager.loadList(ctx);
    }

    @Override
    public LinkedHashSet<User> getUsersByRole(String secRoleName) {

        LinkedHashSet<User> userSet = new LinkedHashSet<>();
        userSet.addAll(findAdminsBySecRole(secRoleName));
        userSet.addAll(getListUsersBySecRole(secRoleName));

        return userSet;
    }

    private List<User> findAdminsBySecRole(String secRoleName) {
        return dataManager.load(User.class)
                .query("select u from sec$User u join u.userRoles ur where ur.roleName = :roleName")
                .parameter("roleName", secRoleName)
                .list();
    }

    private List<User> getListUsersBySecRole(String role) {
        return dataManager.load(User.class)
                .query("select u from sec$User u join u.userRoles ur join ur.role r where r.name = :secRoleName")
                .parameter("secRoleName", role)
                .list();
    }
}